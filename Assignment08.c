/*Assignment 08
Write down a program to store the details of students using the knowledge of loops, arrays and structures
 you gained so far. First name of the student, subject and its marks for each student needs to be recorded.
  Data should be fetched through user input (do not hard code) and print the data back.
Program should record and display information of minimum 5 students.*/


#include<stdio.h>
#define M 30


struct studentDetails
{
    char studentName[M];
    char subjectName[M];
    int subjectMarks;

};

int main()
{
    printf("A program to display the Student Marks\n");
    printf("--------------------------------------\n\n");
    struct studentDetails sd[20];
    int i,num;
    printf("Enter the number of Students to display marks=\n");
    scanf("%d",&num);
    printf("\n\n");
    for(i=0; i<num; i++)
    {
        printf("Enter the name of the Student=");
        scanf("%s",sd[i].studentName);
        printf("\nEnter the Subject Name=");
        scanf("%s",sd[i].subjectName);
        printf("\nEnter the marks of the above Subject=");
        scanf("%d",&(sd[i].subjectMarks));
        printf("\n\n");
    }


    for(i=0; i<num; i++)
    {
        printf("Name of the Student=%s\n",sd[i].studentName);
        printf("Subject=%s\n",sd[i].subjectName);
        printf("Marks=%d\n\n",sd[i].subjectMarks);


    }
    return 0;
}

